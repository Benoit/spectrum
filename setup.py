#!/usr/bin/env python3

from setuptools import setup

# Get the long description from the README file.
with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='kwant-spectrum',
    version='0.0',
    description='Adaptive band structure analyzer for Kwant leads',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://gitlab.kwant-project.org/kwant/spectrum',
    author='T. Kloss, C. W. Groth, X. Waintal, et al.',
    author_email='kloss@itp.uni-frankfurt.de',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3 :: Only',
    ],
    keywords='physics kwant bandstructure',
    py_modules=["kwant_spectrum"],
    python_requires='>=3.5',
    install_requires=['kwant >= 1.4'],
    extras_require={'test': ['pytest'],},
)
