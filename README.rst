Kwant-Spectrum is an extension to `Kwant <http://kwant-project.org/>`_ that
adaptively analyzes band structures of infinite quasi-1d systems.  The
bandstructure is approximated to an arbitrary precision by piecewise cubic
polynomials.  Functionality to deal with special points, intervals, and the
periodic nature of the spectrum is provided.

`Full documentation including a tutorial
<https://kwant-project.org/extensions/spectrum/>`_ is available .  Report bugs
and follow development through the `Kwant-Spectrum repository
<https://gitlab.kwant-project.org/kwant/spectrum>`_.
